import { ApiClient } from '~/plugins/api-client'

let mockedAPIResult: string[]
let apiClient: ApiClient
describe('API client plugin', () => {
  beforeEach(() => {
    apiClient = new ApiClient('https://a-mocked-api-url.com')
    mockedAPIResult = ['mountain 1', 'mountain 2']

    // @ts-ignore
    global.fetch = jest.fn(() => {
      return Promise.resolve({
        json: () => Promise.resolve(mockedAPIResult)
      })
    })
  })

  it('should create the plugin', () => {
    expect(apiClient).toBeTruthy()
  })

  it('should return an array of mountains', async () => {
    expect(await apiClient.getMountains()).toHaveLength(mockedAPIResult.length)
  })

  it('should return a mountain', async () => {
    expect(await apiClient.getMountainWithFetch()).toBe(mockedAPIResult[0])
  })
})
