import { shallowMount } from '@vue/test-utils'
import ItemDetails from '~/components/ItemDetails.vue'

describe('Details component', () => {
  it('should render mountain details ', () => {
    const itemsDetailsComponent = shallowMount(ItemDetails,
      {
        propsData: {
          mountain: {
            title: 'Aconcagua',
            description: 'Aconcagua is a mountain in the Principal Cordillera of the Andes mountain range, in Mendoza Province, Argentina. It is the highest mountain in the Americas and the highest outside of Asia, being the highest in both the Southern and Western Hemispheres with a summit elevation of 6,960.8 metres.',
            image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Aconcagua2016.jpg/600px-Aconcagua2016.jpg'
          }
        }
      })
    expect(itemsDetailsComponent.html()).toContain('Aconcagua')
    expect(itemsDetailsComponent.html()).toContain('is a mountain in the Principal')
    expect(itemsDetailsComponent.find('.v-img')).toBeTruthy()
  })
})
