import { mount } from '@vue/test-utils'
import Counter from '~/components/Counter.vue'

describe('Counter component behaviour', () => {
  it('should render the mountains counter', () => {
    const counterComponent = mount(Counter, {
      propsData: {
        mountainsNumber: 7
      }
    })
    const containerHTML = counterComponent.html()

    expect(containerHTML).toContain('Mountains(7)')
  })
  it('should render a zero counter when no props are available', () => {
    // Given or status of the component
    const component = mount(Counter)

    // Then when something happens
    const componentHTML = component.find('h1').html()

    // What I expect
    expect(componentHTML).toContain('Mountains(0)')
  })
})
