import { mount } from '@vue/test-utils'
import Item from '~/components/Item.vue'

describe('Item Component', () => {
  it('should not render the component when the title is empty', () => {
    // Given a component with no props
    const itemComponent = mount(Item, {
      propsData: {
        mountain: { title: '', visible: false }
      }
    })

    // When we get the component HTML
    const componentHTML = itemComponent.html()

    // Then it should be empty
    expect(componentHTML).toBe('')
  })

  it('should render the title', () => {
    // Given a Mountain with a known title
    const mountainTitle = 'a mountain'
    const mountain = {
      title: mountainTitle,
      visible: true
    }
    // And a mounted component with that mountain as props
    const itemComponent = mount(Item,
      {
        propsData: {
          mountain
        }
      })

    // When we get the component HTML
    const componentHTML = itemComponent.html()

    // Then it should contain the mountain title
    expect(componentHTML).toContain(mountainTitle)
  })

  it('should render the title but not be shown', () => {
    // Given a Mountain with a known title
    const mountainTitle = 'a mountain'
    const mountain = {
      title: mountainTitle,
      visible: false
    }
    // And a mounted component with that mountain as props
    const itemComponent = mount(Item,
      {
        propsData: {
          mountain
        }
      })

    // When we get the component HTML
    const componentHTML = itemComponent.html()

    // Then it should contain the mountain title
    expect(componentHTML).toContain(mountainTitle)
    // Getting an item and checking if its visible
    expect(itemComponent.find('[data-test-id="item"]').isVisible()).toBeFalsy()
    // Getting all the items that are visible
    expect(itemComponent.findAll('[data-test-id="item"]')).toHaveLength(1)
    // Getting all the items and checking that he first one is visible
    expect(itemComponent.findAll('[data-test-id="item"]').at(0).isVisible()).toBeFalsy()
  })
})
