import { mount, shallowMount } from '@vue/test-utils'
import ItemsList from '~/components/ItemsList.vue'
import Item from '~/components/Item.vue'

describe('List component', () => {
  it('should render a empty message if no mountains are given', () => {
    const itemsListComponent = mount(ItemsList)
    expect(itemsListComponent.html()).toContain('No mountains found')
  })

  it('should render one item', () => {
    const itemsListComponent = mount(ItemsList,
      {
        propsData: {
          mountains: [
            { title: 'a mountain', visible: true }
          ]
        }
      })

    expect(itemsListComponent.findAllComponents(Item).length).toBe(1)
  })

  describe('Given a list of mountains', function () {
    const mountains = [
      { title: 'a mountain', visible: true },
      { title: 'an mountain', visible: true },
      { title: 'an awesome mountain', visible: true }
    ]

    it('should render a list of items', () => {
      const itemsListComponent = mount(ItemsList,
        {
          propsData: {
            mountains
          }
        })

      expect(itemsListComponent.findAll('li').length).toBe(3)
    })

    it('should render a list of items', () => {
      const itemsListComponent = shallowMount(ItemsList,
        {
          propsData: {
            mountains
          }
        })

      expect(itemsListComponent.findAllComponents(Item).length).toBe(3)
    })
  })
})
