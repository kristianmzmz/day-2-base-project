import { shallowMount } from '@vue/test-utils'
import IndexPage from '~/pages/index.vue'
import Counter from '~/components/Counter.vue'
import ItemsList from '~/components/ItemsList.vue'

describe('Index page component', function () {
  it('should render all the components', function () {
    const indexPage = shallowMount(IndexPage)

    expect(indexPage.findComponent(Counter)).toBeTruthy()
    expect(indexPage.findComponent(ItemsList)).toBeTruthy()
  })
})
