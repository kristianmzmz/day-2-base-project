import { mount, Wrapper } from '@vue/test-utils'
import DetailsPage from '~/pages/details/_slug.vue'

let detailsPageWrapper: Wrapper<DetailsPage>
describe('Details page', () => {
  it('should render the page as designed', () => {
    detailsPageWrapper = mount(DetailsPage)
    expect(detailsPageWrapper.html()).toMatchSnapshot()
  })

  it('should render a empty message if no mountain is provided', () => {
    const $route = {
      slug: ''
    }
    detailsPageWrapper = mount(
      DetailsPage, {
        mocks: {
          $route
        }
      })

    expect(detailsPageWrapper.html()).toContain('The mountain was not found')
  })

  it.skip('should try to find the mountain if one is provided', () => {
    const $route = {
      slug: 'a-mountain'
    }
    const mockedGetMountains = jest.fn()
    const $apiClient = {
      getMountains: mockedGetMountains
    }
    detailsPageWrapper = mount(DetailsPage, {
      mocks: {
        $route,
        $apiClient
      }
    })

    expect(mockedGetMountains).toHaveBeenCalled()
  })
})
