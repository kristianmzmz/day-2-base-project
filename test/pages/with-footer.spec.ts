import { mount, shallowMount } from '@vue/test-utils'
import Counter from '~/components/Counter.vue'
import ItemsList from '~/components/ItemsList.vue'
import WithFooter from '~/pages/with-footer.vue'

describe('With footer page component', function () {
  it('should render all the components', function () {
    const withFooter = shallowMount(WithFooter)

    expect(withFooter.findComponent(Counter)).toBeTruthy()
    expect(withFooter.findComponent(ItemsList)).toBeTruthy()
  })

  it('should match the snapshot', function () {
    const withFooter = shallowMount(WithFooter)

    expect(withFooter.html()).toMatchSnapshot()
  })

  it('should match the snapshot with mount', function () {
    const withFooter = mount(WithFooter)

    expect(withFooter.html()).toMatchSnapshot()
  })
})
